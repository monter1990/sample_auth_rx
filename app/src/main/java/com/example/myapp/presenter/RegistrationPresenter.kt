package com.example.myapp.presenter

import com.example.myapp.view.RegHomeInterface
import com.example.myapp.interactor.UserInteractor
import com.example.myapp.model.UserModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RegistrationPresenter(private var regHomeInterface: RegHomeInterface?, private val userInteractor: UserInteractor){

    private var compositeDisposable = CompositeDisposable()

    fun sendUser(userModel: UserModel){
        compositeDisposable.add(
            userInteractor.postUserData(userModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ loginResponse ->
                regHomeInterface?.proceedSuccess(loginResponse.login!!)

            },
            { err ->
                regHomeInterface?.showDataFailed(err.localizedMessage)
            })
        )
    }

    fun onDestroy(){
        if (!compositeDisposable.isDisposed()) compositeDisposable.dispose()
        regHomeInterface = null
    }

}