package com.example.myapp.model

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
data class UserModel(
    var login: String? = null,
    var password: String? = null
){
    fun toObject(stringValue: String): UserModel {
        return Json.parse(serializer(), stringValue)
    }

    fun toJson(field: UserModel): String {
        return Json.stringify(serializer(), field)
    }

}