package com.example.myapp.view

import android.view.View

interface RegHomeInterface {

//    fun showProgress() // implement
//    fun hideProgress() // implement

    fun proceedSuccess(login : String)
    fun showDataFailed(strError: String)
    fun showValidationMessage(errorCode: Int)

}

interface onClickListener {
    fun onRegisterButtonClick(v: View)
}