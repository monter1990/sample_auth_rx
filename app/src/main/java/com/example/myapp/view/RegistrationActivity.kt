package com.example.myapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.example.myapp.R
import com.example.myapp.interactor.UserInteractor
import com.example.myapp.model.UserModel
import com.example.myapp.presenter.RegistrationPresenter
import com.example.myapp.utility.ValidationFlags

class RegistrationActivity : AppCompatActivity(), RegHomeInterface, onClickListener {
    private lateinit var registrationPresenter: RegistrationPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        registrationPresenter = RegistrationPresenter(this, UserInteractor())
    }

    override fun onResume() {
        super.onResume()
//        registrationPresenter.checkUser()
    }

    override fun onRegisterButtonClick(v : View) {
        when(true){
            findViewById<EditText>(R.id.reg_form_login).text.isBlank() -> {
                showValidationMessage(ValidationFlags.LOGIN_EMPTY)
                return
            }
            findViewById<EditText>(R.id.reg_form_login).text.count() < 2 -> {
                showValidationMessage(ValidationFlags.LOGIN_TOO_SHORT)
                return
            }
            findViewById<EditText>(R.id.reg_form_password).text.isEmpty() -> {
                showValidationMessage(ValidationFlags.PASSWORD_EMPTY)
                return
            }
        }
        registrationPresenter.sendUser(UserModel(findViewById<EditText>(R.id.reg_form_login).text.toString(), findViewById<EditText>(R.id.reg_form_password).text.toString()))
    }

    override fun showValidationMessage(errorCode: Int) {
        when (errorCode) {
            ValidationFlags.LOGIN_EMPTY -> Toast.makeText(this, getString(R.string.login_field_empty), Toast.LENGTH_LONG).show()
            ValidationFlags.LOGIN_TOO_SHORT -> Toast.makeText(this, getString(R.string.login_too_short), Toast.LENGTH_LONG).show()
            ValidationFlags.PASSWORD_EMPTY -> Toast.makeText(this, getString(R.string.password_empty), Toast.LENGTH_LONG).show()
            ValidationFlags.SERVER_ERROR -> Toast.makeText(this, getString(R.string.server_error), Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroy() {
        registrationPresenter.onDestroy()
        super.onDestroy()
    }

    override fun proceedSuccess(login : String) {
        Toast.makeText(this, "Hello " + login, Toast.LENGTH_LONG).show()
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    override fun showDataFailed(strError: String) {
//        Toast.makeText(this, "Error -> $strError", Toast.LENGTH_LONG).show()
        showValidationMessage(ValidationFlags.SERVER_ERROR)
    }
}