package com.example.myapp.interactor

import android.util.Log
import com.example.myapp.model.UserModel
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import kotlinx.io.IOException
import okhttp3.*
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.util.*

class UserInteractor {

    fun postUserData(payload: UserModel) : Single<UserModel> {
        val body = payload.toJson(payload).toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
        val client = OkHttpClient();
        val request = Request.Builder()
            .url("http://10.0.2.2:3000/auth/reg")
            .method("POST", body)
            .build();
        Log.d("INFO", "request-> $request")
        return Single.create {
             client.newCall(request).enqueue(object: Callback {
                 override fun onResponse(call: Call, response: Response) {
                     it.onSuccess(UserModel().toObject(response.body!!.string()))
                 }
                 override fun onFailure(call: Call, e: java.io.IOException) {
                     it.onError(e)
                 }
             })
         }
    }

}