package com.example.myapp.utility

object ValidationFlags {
    internal val LOGIN_EMPTY = 1
    internal val LOGIN_TOO_SHORT = 2
    internal val PASSWORD_EMPTY = 3
    internal val SERVER_ERROR = 4
}